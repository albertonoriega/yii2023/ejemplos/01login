<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Cliente $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="cliente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idCliente')->input('number') ?>

    <?= $form->field($model, 'nombreCliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellidosCliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'correoCliente')->input('email') ?>

    <?= $form->field($model, 'fechaNacimiento')->input('date') ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>