<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property int $idCliente
 * @property string|null $nombreCliente
 * @property string|null $apellidosCliente
 * @property string|null $correoCliente
 * @property string|null $fechaNacimiento
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idCliente'], 'required'],
            [['idCliente'], 'integer'],
            [['fechaNacimiento'], 'date', 'format' => 'yyyy-MM-dd'],
            [['nombreCliente', 'apellidosCliente'], 'string', 'max' => 100],
            [['correoCliente'], 'email'],
            [['idCliente'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idCliente' => 'ID',
            'nombreCliente' => 'Nombre',
            'apellidosCliente' => 'Apellidos',
            'correoCliente' => 'Correo',
            'fechaNacimiento' => 'Fecha Nacimiento',
        ];
    }
}
